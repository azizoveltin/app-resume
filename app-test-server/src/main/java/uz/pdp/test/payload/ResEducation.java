package uz.pdp.test.payload;

import lombok.Data;

import java.util.Date;

@Data
public class ResEducation {
    private Date graduatedYear;
    private String graduatedPlace;
    private String specialization;
}
