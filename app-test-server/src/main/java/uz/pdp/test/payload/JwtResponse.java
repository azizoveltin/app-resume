package uz.pdp.test.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse {
    private String tokenType="Bearer";
    private String tokenBody;

    public JwtResponse(String tokenBody) {
        this.tokenBody = tokenBody;
    }
}
