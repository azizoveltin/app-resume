package uz.pdp.test.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResResume {
    private String fio;
    private String birthDate;
    private String nationality;
    private String educationLevel;
    private List<ReqEducation> reqEducations;
    private String phoneNumber;
    private String email;

}
