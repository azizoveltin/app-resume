package uz.pdp.test.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqWork {
    private String workFrom;
    private String workTo;
    private String workName;
}
