package uz.pdp.test.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqResume {
    private Integer id;
    private String fio;
    private String birthDate;
    private String nationality;
    private String educationLevel;
    private List<ReqEducation> reqEducations;
//    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "phone number must be 13 digits")
    private String phoneNumber;
//    @Pattern(regexp = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$", message = "Email not validated")
    private String email;
    private List<ReqWork> reqWorks;
}
