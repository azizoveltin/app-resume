package uz.pdp.test.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uz.pdp.test.entity.User;
import uz.pdp.test.payload.ApiResponse;
import uz.pdp.test.payload.JwtResponse;
import uz.pdp.test.payload.ReqUser;
import uz.pdp.test.payload.Reqlogin;
import uz.pdp.test.security.AuthService;
import uz.pdp.test.security.CurrentUser;
import uz.pdp.test.security.JwtTokenProvider;


@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenProvider jwtTokenProvider;
    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public HttpEntity<?> signIn(@RequestBody Reqlogin reqLogin) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(reqLogin.getPhoneNumber(), reqLogin.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @PostMapping("/register")
    public HttpEntity<?> signUp(@RequestBody ReqUser reqUser) {
        ApiResponse response = authService.addUser(reqUser);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }


//    @PutMapping("/{id}")
//    private HttpEntity<?> editUser(@PathVariable UUID id, @RequestBody ReqUser reqUser) {
//        ApiResponse response = authService.editUser(id, reqUser);
//        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
//    }


}
