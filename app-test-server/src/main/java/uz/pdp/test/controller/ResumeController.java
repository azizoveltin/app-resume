package uz.pdp.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.test.entity.Resume;
import uz.pdp.test.entity.User;
import uz.pdp.test.payload.ApiResponse;
import uz.pdp.test.payload.ReqResume;
import uz.pdp.test.repository.ResumeRepository;
import uz.pdp.test.security.CurrentUser;
import uz.pdp.test.service.ResumeService;

@RestController
@RequestMapping("/api/resume")
public class ResumeController {
    @Autowired
    ResumeService resumeService;
    @Autowired
    ResumeRepository resumeRepository;

    @PostMapping
    public HttpEntity<?> add(@RequestBody ReqResume reqResume) {
        ApiResponse response = resumeService.addResume(reqResume);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable Integer id, @RequestBody ReqResume reqResume) {
        ApiResponse response = resumeService.editResume(id, reqResume);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/me")
    public HttpEntity<?> getUser(@CurrentUser User user) {
        return ResponseEntity.ok(new ApiResponse("Mana user", true, user));
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id) {
        ApiResponse response = resumeService.deleteResume(id);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getResume(@PathVariable Integer id) {
        Resume resume = resumeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getResume"));
        return ResponseEntity.ok(resumeService.getResume(resume));
    }

    @GetMapping("/all")
    public HttpEntity<?> getResumies() {
        return ResponseEntity.ok(resumeService.getResumies());
    }

}
