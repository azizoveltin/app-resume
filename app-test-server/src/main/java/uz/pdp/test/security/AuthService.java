package uz.pdp.test.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.test.entity.User;
import uz.pdp.test.entity.enums.RoleName;
import uz.pdp.test.payload.ApiResponse;
import uz.pdp.test.payload.ReqUser;
import uz.pdp.test.repository.RoleRepository;
import uz.pdp.test.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse addUser(ReqUser reqUser) {

        if (!userRepository.existsByPhoneNumber(reqUser.getPhoneNumber())) {

                User user = new User();
                user.setFirstName(reqUser.getFirstName());
                user.setLastName(reqUser.getLastName());
                user.setPhoneNumber(reqUser.getPhoneNumber());
                user.setBirthDate(reqUser.getBirthDate());
                user.setEmail(reqUser.getEmail());
                user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
                user.setRoles(Collections.singletonList(roleRepository.findByName(RoleName.ROLE_USER)));
                userRepository.save(user);
                return new ApiResponse("User ro'yxatdan o'tkazildi", true);


        }
        return new ApiResponse("Bunday telefon raqamli user sistemada mavjud", false);
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(s).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

    public UserDetails loadUserByUserId(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }


}
