package uz.pdp.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.test.entity.Education;
import uz.pdp.test.entity.Resume;
import uz.pdp.test.entity.Work;
import uz.pdp.test.payload.*;
import uz.pdp.test.repository.EducationRepository;
import uz.pdp.test.repository.ResumeRepository;
import uz.pdp.test.repository.WorkRepository;
import uz.pdp.test.utils.BaseUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ResumeService {
    @Autowired
    EducationRepository educationRepository;
    @Autowired
    WorkRepository workRepository;
    @Autowired
    ResumeRepository resumeRepository;
    @Autowired
    BaseUtils utils;


    public ApiResponse addResume(ReqResume reqResume) {
        try {
            Resume resume = new Resume();
            resume.setFio(reqResume.getFio());
            resume.setBirthDate(utils.toLocalDate(reqResume.getBirthDate()));
            resume.setNationality(reqResume.getNationality());
            resume.setEducationLevel(reqResume.getEducationLevel());
            resume.setPhoneNumber(reqResume.getPhoneNumber());
            resume.setEmail(reqResume.getEmail());
            Resume savedResume = resumeRepository.save(resume);

            for (ReqEducation reqEducation : reqResume.getReqEducations()) {
                Education education = new Education();
                education.setResume(savedResume);
                education.setGraduatedYear(utils.toLocalDate(reqEducation.getGraduatedYear()));
                education.setSpecialization(reqEducation.getSpecialization());
                education.setGraduatedPlace(reqEducation.getGraduatedPlace());
                educationRepository.save(education);
            }

            for (ReqWork reqWork : reqResume.getReqWorks()) {
                Work work = new Work();
                work.setResume(savedResume);
                work.setWorkFrom(utils.toLocalDate(reqWork.getWorkFrom()));
                work.setWorkName(reqWork.getWorkName());
                work.setWorkTo(utils.toLocalDate(reqWork.getWorkTo()));
                workRepository.save(work);
            }

            return new ApiResponse("Resume saved", true);

        } catch (Exception e) {
            return new ApiResponse("Resume not saved", false);
        }
    }


    public ApiResponse deleteResume(Integer id) {
        if (resumeRepository.existsById(id)) {
            resumeRepository.deleteById(id);
            return new ApiResponse("Resume deleted", true);
        }
        return new ApiResponse("Resume not found", false);
    }

    public ResResume getResume(Resume resume) {
        return new ResResume(
                resume.getFio(),
                resume.getBirthDate().toString(),
                resume.getNationality(),
                resume.getEducationLevel(),
                resume.getEducations().stream().map(this::getEducation).collect(Collectors.toList()),
                resume.getPhoneNumber(),
                resume.getEmail()
        );

    }

    public ReqEducation getEducation(Education education) {
        return new ReqEducation(
                education.getGraduatedYear().toString(),
                education.getGraduatedPlace(),
                education.getSpecialization()
        );
    }


    public List<ResResume> getResumies() {
        return resumeRepository.findAll().stream().map(this::getResume).collect(Collectors.toList());
    }


    public ApiResponse editResume(Integer id, ReqResume reqResume) {

        try {
            Optional<Resume> optionalResume = resumeRepository.findById(id);
            if (optionalResume.isPresent()) {
                Resume resume = optionalResume.get();
                resume.setFio(reqResume.getFio());
                resume.setBirthDate(utils.toLocalDate(reqResume.getBirthDate()));
                resume.setNationality(reqResume.getNationality());
                resume.setEducationLevel(reqResume.getEducationLevel());
                resume.setPhoneNumber(reqResume.getPhoneNumber());
                resume.setEmail(reqResume.getEmail());
                Resume savedResume = resumeRepository.save(resume);


                List<Education> educations = educationRepository.findAllByResumeId(resume.getId());
                educationRepository.deleteAll(educations);

                for (ReqEducation reqEducation : reqResume.getReqEducations()) {
                    Education education = new Education();
                    education.setResume(savedResume);
                    education.setGraduatedYear(utils.toLocalDate(reqEducation.getGraduatedYear()));
                    education.setSpecialization(reqEducation.getSpecialization());
                    education.setGraduatedPlace(reqEducation.getGraduatedPlace());
                    educationRepository.save(education);

                }

                List<Work> works = workRepository.findAllByResumeId(resume.getId());
                workRepository.deleteAll(works);

                for (ReqWork reqWork : reqResume.getReqWorks()) {
                    Work work = new Work();
                    work.setResume(savedResume);
                    work.setWorkFrom(utils.toLocalDate(reqWork.getWorkFrom()));
                    work.setWorkName(reqWork.getWorkName());
                    work.setWorkTo(utils.toLocalDate(reqWork.getWorkTo()));
                    workRepository.save(work);

                }

                return new ApiResponse("Resume edited", true);
            }
            return new ApiResponse("Resume not found ", false);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}