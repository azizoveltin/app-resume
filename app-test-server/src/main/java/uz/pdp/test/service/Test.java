package uz.pdp.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.test.entity.Education;
import uz.pdp.test.entity.Resume;
import uz.pdp.test.entity.Work;
import uz.pdp.test.payload.*;
import uz.pdp.test.repository.EducationRepository;
import uz.pdp.test.repository.ResumeRepository;
import uz.pdp.test.repository.WorkRepository;

import java.util.List;
import java.util.stream.Collectors;

public class Test {

//    @Service
//    public class ResumeService {
//        @Autowired
//        EducationRepository educationRepository;
//        @Autowired
//        WorkRepository workRepository;
//        @Autowired
//        ResumeRepository resumeRepository;
//
//        //ad ,edit
//        @Transactional
//        public ApiResponse addResume(ReqResume reqResume) {
//            try {
//                Resume resume = new Resume();
//                if (reqResume.getId() != null) {
//                    resume = resumeRepository.findById(reqResume.getId()).orElseThrow(() ->
//                            new ResourceNotFoundException("getResume"));
//                    List<Education> educationList = resume.getEducations();
//                    for (Education education : educationList) {
//                        educationRepository.delete(education);
//
//                    }
//                    List<Work> workList = resume.getWorks();
//                    for (Work work : workList) {
//                        workRepository.delete(work);
//                        try {
//                            workRepository.delete(work);
//                        } catch (Exception ignored) {
//
//                        }
//                    }
//                }
//                resume.setFio(reqResume.getFio());
//                resume.setBirthDate(reqResume.getBirthDate());
//                resume.setNationality(reqResume.getNationality());
//                resume.setEducationLevel(reqResume.getEducationLevel());
//                resume.setPhoneNumber(reqResume.getPhoneNumber());
//                resume.setEmail(reqResume.getEmail());
//
//                Resume savedResume = resumeRepository.save(resume);
//
//                for (ReqEducation reqEducation : reqResume.getReqEducations()) {
//                    Education education = new Education();
//                    education.setResume(savedResume);
//                    education.setGraduatedYear(reqEducation.getGraduatedYear());
//                    education.setSpecialization(reqEducation.getSpecialization());
//                    education.setGraduatedPlace(reqEducation.getGraduatedPlace());
//                    educationRepository.save(education);
//                }
//
//                for (ReqWork reqWork : reqResume.getReqWorks()) {
//                    Work work = new Work();
//                    work.setResume(savedResume);
//                    work.setWorkFrom(reqWork.getWorkFrom());
//                    work.setWorkName(reqWork.getWorkName());
//                    work.setWorkTo(reqWork.getWorkTo());
//                    workRepository.save(work);
//                }
//
//
//                return new ApiResponse(reqResume.getId() == null ? "Resume saved" : "Resume edited", true);
//
//            } catch (Exception e) {
//                return new ApiResponse("Resume not saved", false);
//            }
//        }
//
//
//        public ApiResponse deleteResume(Integer id) {
//            if (resumeRepository.existsById(id)) {
//                resumeRepository.deleteById(id);
//                return new ApiResponse("Resume deleted", true);
//            }
//            return new ApiResponse("Resume not found", false);
//        }
//
//        public ResResume getResume(Resume resume) {
//            return new ResResume(
//                    resume.getFio(),
//                    resume.getBirthDate(),
//                    resume.getNationality(),
//                    resume.getEducationLevel(),
//                    resume.getEducations().stream().map(this::getEducation).collect(Collectors.toList()),
//                    resume.getPhoneNumber(),
//                    resume.getEmail()
//            );
//
//        }
//
//        public ReqEducation getEducation(Education education) {
//            return new ReqEducation(
//                    education.getGraduatedYear(),
//                    education.getGraduatedPlace(),
//                    education.getSpecialization()
//            );
//        }
//
//
//        public List<ResResume> getResumies() {
//            return resumeRepository.findAll().stream().map(this::getResume).collect(Collectors.toList());
//        }
//    }

}
