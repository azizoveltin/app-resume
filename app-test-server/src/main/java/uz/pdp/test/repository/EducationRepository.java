package uz.pdp.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.test.entity.Education;

import java.util.List;


public interface EducationRepository extends JpaRepository<Education,Integer> {
    List<Education> findAllByResumeId(Integer id);
}
