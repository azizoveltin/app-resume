package uz.pdp.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.test.entity.Role;
import uz.pdp.test.entity.enums.RoleName;

@RepositoryRestResource(path = "/role")

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(RoleName roleUser);
}
