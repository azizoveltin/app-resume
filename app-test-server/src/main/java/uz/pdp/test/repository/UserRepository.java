package uz.pdp.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.test.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumber(String s);
    boolean existsByPhoneNumber(String phoneNumber);
}
