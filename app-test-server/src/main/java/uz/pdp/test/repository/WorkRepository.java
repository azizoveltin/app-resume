package uz.pdp.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.test.entity.Work;

import java.util.List;

public interface WorkRepository extends JpaRepository<Work,Integer> {
    List<Work> findAllByResumeId(Integer id);
}
