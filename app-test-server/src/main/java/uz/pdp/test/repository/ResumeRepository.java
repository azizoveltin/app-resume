package uz.pdp.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.test.entity.Resume;

public interface ResumeRepository extends JpaRepository<Resume,Integer> {
}
