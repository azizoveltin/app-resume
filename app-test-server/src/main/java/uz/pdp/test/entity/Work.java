package uz.pdp.test.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Work {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDateTime workFrom;
    private LocalDateTime workTo;
    private String workName;

    @ManyToOne(fetch = FetchType.LAZY)
    private Resume resume;
}
