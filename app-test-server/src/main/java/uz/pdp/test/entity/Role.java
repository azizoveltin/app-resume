package uz.pdp.test.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import uz.pdp.test.entity.enums.RoleName;

import javax.persistence.*;
@Entity
@Data
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    private RoleName name;

    @Override
    public String getAuthority() {
        return name.name();
    }
}
